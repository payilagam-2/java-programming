package LearnArrayList;

import java.util.ArrayList;

public class ArrayListDemo {

 public static void main(String[] args) {
  // TODO Auto-generated method stub
ArrayList al = new ArrayList();
al.add("selva");
al.add("saran");
al.add("10");
al.add("10.5f");
al.add("true");
System.out.println(al);
al.add(2,"abc");
System.out.println(al);
System.out.println(al.contains(10.5f));
System.out.println(al.indexOf("xyz"));
System.out.println(al.isEmpty());
al.add("saran");
System.out.println(al.lastIndexOf("saran"));
System.out.println(al);
System.out.println(al.indexOf("saran"));
al.remove(6);
System.out.println(al);
al.remove("abc");
System.out.println(al);
Object[] ob = al.toArray();
for(int i=0;i<ob.length;i++)
  System.out.println(ob[i]);
ArrayList al2 = new ArrayList();
al2.addAll(al);
System.out.println("al2 contains=> "+al2);
al2.add("jawahar");
al2.add("kannan");
System.out.println("al contains=> "+al);
System.out.println("al2 contains=> "+al2);
System.out.println(al2.containsAll(al));
al2.retainAll(al);
System.out.println("al2 contains=> "+al2);

 }

}
